package common

type ValidationError struct {
	err string
}

func NewValidationError(err string) ValidationError {
	return ValidationError{err: err}
}

func (e ValidationError) Error() string {
	return e.err
}

type InternalServerError struct {
	err string
}

func NewInternalServerError(err string) InternalServerError {
	return InternalServerError{err: err}
}

func (e InternalServerError) Error() string {
	return e.err
}

type NotFoundError struct {
	err string
}

func NewNotFoundError(err string) NotFoundError {
	return NotFoundError{err: err}
}

func (e NotFoundError) Error() string {
	return e.err
}

type UnauthorizedError struct {
	err string
}

func NewUnauthorizedError(err string) UnauthorizedError {
	return UnauthorizedError{err: err}
}

func (e UnauthorizedError) Error() string {
	return e.err
}

type BadRequestError struct {
	err string
}

func NewBadRequestError(err string) BadRequestError {
	return BadRequestError{err: err}
}

func (e BadRequestError) Error() string {
	return e.err
}
