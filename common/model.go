package common

type DefaultResponse[T any] struct {
	Code    int    `json:"code"`
	Message string `json:"status"`
	Data    T      `json:"data"`
}
