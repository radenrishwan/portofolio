package common

import (
	"log"
	"time"
)

func ErrorLog(err error) {
	now := time.Now().Format("2006-01-02 15:04:05")
	log.Printf("[%s][error] %s\n", now, err.Error())
}
