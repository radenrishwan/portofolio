package common

import "time"

func GenerateCurrentMili() uint64 {
	return uint64(time.Now().UnixMilli())
}
