# Portofolio Website
my own portofolio website. this project is using golang as backend and tailwind as frontend. this project is still under development

# Requirement
you need to install js runtime & go to run this project. if you use js runtime other than `bun`, you need to change command on makefile

## Installation
- install bun (you can use another js runtime like `node` or `deno`, but you need to change command on makefile)
- install go
- install make (if using windows, you can use `mingw`)
- edit `Makefile` to match your environment like url, port, etc

## Command
- initial project

install dependency
```bash
make init
```

- run tailwind

generate tailwind output
```bash
make tailwind
```

- run project

run golang server
```bash
make run
```

- build project

build project
```bash
make build
```