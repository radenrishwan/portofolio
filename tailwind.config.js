/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./static/**/*.{html,js,css}",
  ],
  daisyui: {
    themes: [
      {
        mytheme: {
          "primary": "#e5e16b",
          "secondary": "#1fc4bc",
          "accent": "#53c619",
          "neutral": "#28222a",
          "base-100": "#f0f0f0",
          "info": "#96c4f8",
          "success": "#22b9ad",
          "warning": "#edad5a",
          "error": "#dd2f2c",
        },
      },
    ],
  },
  plugins: [
    require('@tailwindcss/typography'),
    require("daisyui"),
    require('@tailwindcss/line-clamp'),
  ],
}

