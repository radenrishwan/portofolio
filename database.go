package portofolio

import (
	"context"
	"log"

	_ "github.com/lib/pq"
	"github.com/radenrishwan/portofolio/ent"
)

func CreatePostgresDB() *ent.Client {
	uri := "postgres://avnadmin:AVNS_MDE3nEQRw6B0oK74p8z@portofolio-portofolio.aivencloud.com:11855/defaultdb?sslmode=verify-ca;sslrootcert=ca.pem"
	client, err := ent.Open("postgres", uri)

	if err != nil {
		log.Fatalf("failed opening connection to postgres: %v", err)
	}

	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	return client
}
