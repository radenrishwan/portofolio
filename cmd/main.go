package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	"github.com/gofiber/template/html/v2"
	"github.com/radenrishwan/portofolio"
	"github.com/radenrishwan/portofolio/modules/admin"
	"github.com/radenrishwan/portofolio/modules/articles"
	"github.com/radenrishwan/portofolio/modules/static"
)

func main() {
	db := portofolio.CreatePostgresDB()
	engine := html.New("./static", ".html")

	app := fiber.New(fiber.Config{
		Views: engine,
	})

	// metrics
	app.Get("/metrics", monitor.New(monitor.Config{
		Title: "Fiber Monitor",
	}))

	// handler
	articleHandler := articles.NewArticleRouter(app, articles.NewArticleHandler(db))
	staticHandler := static.NewStaticRouter(app, static.NewStaticHandler())
	adminHandler := admin.NewAdminRouter(app)

	// binding routes
	articleHandler.RegisterRoutes()
	staticHandler.RegisterRoutes()
	adminHandler.RegisterRoutes()

	app.Listen(":3000")
}
