tailwind:
	bunx tailwindcss -i ./static/css/input.css -o ./static/css/style.css --watch

run:
	air

init:
	go mod tidy
	bun install

build:
	go build -o bin/main cmd/main.go