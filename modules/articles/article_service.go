package articles

import (
	"context"

	"entgo.io/ent/dialect/sql"
	"github.com/radenrishwan/portofolio/common"
	"github.com/radenrishwan/portofolio/ent"
	"github.com/radenrishwan/portofolio/ent/article"
)

type ArticleService struct {
	*ent.Client
}

func NewArticleService(client *ent.Client) *ArticleService {
	return &ArticleService{
		Client: client,
	}
}

func (service *ArticleService) Create(ctx context.Context, request ArticleCreateRequest) (common.DefaultResponse[ArticleCreateResponse], error) {
	// generate time now
	now := common.GenerateCurrentMili()

	article, err := service.Client.Article.Create().
		SetTitle(request.Title).
		SetContent(request.Content).
		SetCreatedAt(now).
		SetUpdatedAt(now).
		Save(ctx)

	if err != nil {
		common.ErrorLog(err)
		return common.DefaultResponse[ArticleCreateResponse]{}, common.NewInternalServerError("failed to create article")
	}

	return common.DefaultResponse[ArticleCreateResponse]{
		Code:    201,
		Message: "success create article",
		Data: ArticleCreateResponse{
			Id:        article.ID,
			Title:     article.Title,
			Content:   article.Content,
			CreatedAt: article.CreatedAt,
			UpdatedAt: article.UpdatedAt,
		},
	}, nil
}

func (service *ArticleService) GetById(ctx context.Context, id int) (common.DefaultResponse[ArticleGetByIdResponse], error) {
	article, err := service.Client.Article.Get(ctx, id)
	if err != nil {
		common.ErrorLog(err)
		return common.DefaultResponse[ArticleGetByIdResponse]{}, common.NewNotFoundError("failed to get article")
	}

	response := common.DefaultResponse[ArticleGetByIdResponse]{
		Code:    200,
		Message: "success get article",
		Data: ArticleGetByIdResponse{
			Id:        article.ID,
			Title:     article.Title,
			Content:   article.Content,
			CreatedAt: article.CreatedAt,
			UpdatedAt: article.UpdatedAt,
		},
	}

	return response, nil
}

func (service *ArticleService) GetAll(
	ctx context.Context,
	limit int,
	offset int,
	order string,
) (common.DefaultResponse[[]ArticleGetAllResponse], error) {
	// get limit, offset, and order from request query
	if order != "asc" && order != "desc" && order != "created_at" && order != "updated_at" {
		common.ErrorLog(common.NewValidationError("invalid order"))
		return common.DefaultResponse[[]ArticleGetAllResponse]{}, common.NewValidationError("invalid order, must be asc or desc")
	}

	var orderType article.OrderOption
	switch order {
	case "asc":
		orderType = article.ByTitle(sql.OrderAsc())
	case "desc":
		orderType = article.ByTitle(sql.OrderDesc())
	case "created_at":
		orderType = article.ByCreatedAt()
	case "updated_at":
		orderType = article.ByUpdatedAt()
	default:
		orderType = article.ByCreatedAt(sql.OrderAsc())

	}

	// get all article
	articles, err := service.Client.Article.Query().Limit(limit).Offset(offset).Order(orderType).All(ctx)
	if err != nil {
		common.ErrorLog(err)
		return common.DefaultResponse[[]ArticleGetAllResponse]{}, common.NewInternalServerError("failed to get all article")
	}

	// map article to response
	var data []ArticleGetAllResponse
	for _, article := range articles {
		data = append(data, ArticleGetAllResponse{
			Id:        article.ID,
			Title:     article.Title,
			Content:   article.Content,
			CreatedAt: article.CreatedAt,
			UpdatedAt: article.UpdatedAt,
		})
	}

	result := common.DefaultResponse[[]ArticleGetAllResponse]{
		Code:    200,
		Message: "success get articles",
		Data:    data,
	}

	return result, nil
}

func (service *ArticleService) Update(ctx context.Context, request ArticleUpdateRequest) (common.DefaultResponse[ArticleUpdateResponse], error) {
	// generate time now
	now := common.GenerateCurrentMili()

	// update article
	article, err := service.Client.Article.UpdateOneID(request.Id).
		SetTitle(request.Title).
		SetContent(request.Content).
		SetUpdatedAt(now).
		Save(ctx)

	if err != nil {
		common.ErrorLog(err)
		return common.DefaultResponse[ArticleUpdateResponse]{}, common.NewInternalServerError("failed to update article")
	}

	response := common.DefaultResponse[ArticleUpdateResponse]{
		Code:    200,
		Message: "success update article",
		Data: ArticleUpdateResponse{
			Id:        article.ID,
			Title:     article.Title,
			Content:   article.Content,
			CreatedAt: article.CreatedAt,
			UpdatedAt: article.UpdatedAt,
		},
	}

	return response, nil
}

func (service *ArticleService) Delete(ctx context.Context, id int) (common.DefaultResponse[ArticleDeleteResponse], error) {
	// delete article
	err := service.Client.Article.DeleteOneID(id).Exec(ctx)
	if err != nil {
		common.ErrorLog(err)
		return common.DefaultResponse[ArticleDeleteResponse]{}, common.NewInternalServerError("failed to delete article")
	}

	response := common.DefaultResponse[ArticleDeleteResponse]{
		Code:    200,
		Message: "success delete article",
		Data: ArticleDeleteResponse{
			Id: id,
		},
	}

	return response, nil
}
