package articles

import (
	"github.com/gofiber/fiber/v2"
	"github.com/radenrishwan/portofolio/common"
	"github.com/radenrishwan/portofolio/ent"
)

type ArticleHandler struct {
	*ent.Client
	*ArticleValidation
	*ArticleService
}

func NewArticleHandler(client *ent.Client) *ArticleHandler {
	return &ArticleHandler{
		Client:            client,
		ArticleValidation: NewArticleValidation(),
	}
}

func (handler *ArticleHandler) Create(ctx *fiber.Ctx) error {
	// TODO: check if user is authenticated

	// get data from request body
	var request ArticleCreateRequest
	if err := ctx.BodyParser(request); err != nil {
		common.ErrorLog(err)
		panic(common.NewBadRequestError("invalid request body"))
	}

	err := handler.ArticleValidation.Create(request)
	if err != nil {
		common.ErrorLog(err)
		panic(err)
	}

	result, err := handler.ArticleService.Create(ctx.Context(), request)
	if err != nil {
		common.ErrorLog(err)
		panic(err)
	}

	return ctx.Status(result.Code).JSON(result)
}

func (handler *ArticleHandler) GetById(ctx *fiber.Ctx) error {
	// get id from request params
	id, err := ctx.ParamsInt("id")
	if err != nil {
		common.ErrorLog(err)
		return common.NewValidationError("invalid id")
	}

	result, err := handler.ArticleService.GetById(ctx.Context(), id)
	if err != nil {
		common.ErrorLog(err)
		panic(err)
	}

	return ctx.Status(result.Code).JSON(result)
}

func (handler *ArticleHandler) GetAll(ctx *fiber.Ctx) error {
	// get limit, offset, and order from request query
	limit := ctx.QueryInt("limit", 5)
	offset := ctx.QueryInt("offset", 0)
	order := ctx.Query("order", "created_at")
	if order != "asc" && order != "desc" && order != "created_at" && order != "updated_at" {
		common.ErrorLog(common.NewValidationError("invalid order"))
		return common.NewValidationError("invalid order, must be asc or desc")
	}

	// get all articles
	result, err := handler.ArticleService.GetAll(ctx.Context(), limit, offset, order)
	if err != nil {
		common.ErrorLog(err)
		panic(err)
	}

	return ctx.Status(result.Code).JSON(result)
}

func (handler *ArticleHandler) Update(ctx *fiber.Ctx) error {
	// get data from request body
	var request ArticleUpdateRequest
	if err := ctx.BodyParser(request); err != nil {
		common.ErrorLog(err)
		return err
	}

	err := handler.ArticleValidation.Update(ctx.Context(), request)
	if err != nil {
		common.ErrorLog(err)
		return err
	}

	response, err := handler.ArticleService.Update(ctx.Context(), request)
	if err != nil {
		common.ErrorLog(err)
		panic(err)
	}

	return ctx.Status(response.Code).JSON(response)
}

func (handler *ArticleHandler) Delete(ctx *fiber.Ctx) error {
	// get id from request params
	id, err := ctx.ParamsInt("id")
	if err != nil {
		common.ErrorLog(err)
		panic(common.NewValidationError("invalid id"))
	}

	response, err := handler.ArticleService.Delete(ctx.Context(), id)
	if err != nil {
		common.ErrorLog(err)
		panic(err)
	}

	return ctx.Status(response.Code).JSON(response)
}
