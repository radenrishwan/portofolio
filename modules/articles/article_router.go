package articles

import (
	"github.com/gofiber/fiber/v2"
	"github.com/radenrishwan/portofolio/modules/auth"
)

type ArticleRouter struct {
	*fiber.App
	*ArticleHandler
	auth.AuthMiddleware
}

func NewArticleRouter(app *fiber.App, handler *ArticleHandler) *ArticleRouter {
	return &ArticleRouter{
		App:            app,
		ArticleHandler: handler,
	}
}

func (r *ArticleRouter) RegisterRoutes() {
	group := r.Group("/api/v1/articles")
	groupWithAuth := r.Group("/api/v1/articles", r.AuthMiddleware.GetAuthKey())

	group.Get("/:id", r.ArticleHandler.GetById)
	group.Get("/", r.ArticleHandler.GetAll)

	groupWithAuth.Post("/", r.ArticleHandler.Create)
	groupWithAuth.Put("/", r.ArticleHandler.Update)
	groupWithAuth.Delete("/:id", r.ArticleHandler.Delete)
}
