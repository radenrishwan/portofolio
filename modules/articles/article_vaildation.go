package articles

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/radenrishwan/portofolio/common"
	"github.com/radenrishwan/portofolio/ent"
	"github.com/radenrishwan/portofolio/ent/article"
)

type ArticleValidation struct {
	*ent.Client
}

func NewArticleValidation() *ArticleValidation {
	return &ArticleValidation{}
}

func (v *ArticleValidation) Create(request ArticleCreateRequest) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Title, validation.Required, validation.Length(1, 255)),
		validation.Field(&request.Content, validation.Required, validation.Length(1, 65535)),
	)

	if err != nil {
		return common.NewValidationError(err.Error())
	}

	return nil
}

func (v *ArticleValidation) Update(ctx context.Context, request ArticleUpdateRequest) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Id, validation.Required, validation.Min(1)),
		validation.Field(&request.Title, validation.Required, validation.Length(1, 255)),
		validation.Field(&request.Content, validation.Required, validation.Length(1, 65535)),
	)

	if err != nil {
		return common.NewValidationError(err.Error())
	}

	// check if article exist
	count, err := v.Client.Article.Query().Where(article.ID(request.Id)).Count(ctx)
	if count < 1 {
		return common.NewNotFoundError("article not found")
	}

	if err != nil {
		return common.NewInternalServerError("failed to update article")
	}

	return nil
}
