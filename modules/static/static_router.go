package static

import "github.com/gofiber/fiber/v2"

type StaticRouter struct {
	*fiber.App
	*StaticHandler
}

func NewStaticRouter(app *fiber.App, handler *StaticHandler) *StaticRouter {
	return &StaticRouter{
		App:           app,
		StaticHandler: handler,
	}
}

func (router *StaticRouter) RegisterRoutes() {
	router.App.Static("/css", "./static/css")
	router.App.Static("/img", "./static/img")
	router.App.Static("/svg", "./static/svg")

	router.App.Get("/", router.StaticHandler.Index)
}
