package static

import "github.com/gofiber/fiber/v2"

type StaticHandler struct{}

func NewStaticHandler() *StaticHandler {
	return &StaticHandler{}
}

func (handler *StaticHandler) Index(ctx *fiber.Ctx) error {
	return ctx.Render("index", fiber.Map{}, "layouts/main")
}
