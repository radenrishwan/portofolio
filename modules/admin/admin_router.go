package admin

import (
	"errors"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/radenrishwan/portofolio/common"
	"github.com/radenrishwan/portofolio/modules/auth"
)

type AdminRouter struct {
	*fiber.App
	*auth.AuthMiddleware
}

func NewAdminRouter(app *fiber.App) *AdminRouter {
	return &AdminRouter{
		App: app,
	}
}

func (router *AdminRouter) RegisterRoutes() {
	router.App.Get("/admin", func(ctx *fiber.Ctx) error {
		return ctx.Render("admin/index", fiber.Map{}, "layouts/main")
	})

	router.App.Post("/admin/login", func(ctx *fiber.Ctx) error {
		key := ctx.FormValue("key")

		if key != "dslkahjskdhaskudhaisidkjas" { // TODO: move into flag
			common.ErrorLog(errors.New("invalid login"))
			return ctx.Redirect("/admin")
		}

		// set cookie value
		ctx.Cookie(&fiber.Cookie{
			Name:    "access_token",
			Value:   "dslkahjskdhaskudhaisidkjas",
			Path:    "/",
			Expires: time.Now().Add(7 * 24 * time.Hour),
		})

		return ctx.Redirect("/admin/dashboard")
	})

	router.App.Post("/admin/logout", func(ctx *fiber.Ctx) error {
		// delete cookie value
		ctx.Cookie(&fiber.Cookie{
			Name:    "access_token",
			Value:   "",
			Path:    "/",
			Expires: time.Now().Add(-1 * time.Hour),
		})

		return ctx.Redirect("/admin")
	})

	groupWithAuth := router.App.Group("/admin", router.AuthMiddleware.GetAuthKey())
	groupWithAuth.Get("/dashboard", func(ctx *fiber.Ctx) error {
		return ctx.Render("admin/dashboard", fiber.Map{}, "layouts/main")
	})
}
