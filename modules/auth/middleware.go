package auth

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/keyauth"
	"github.com/radenrishwan/portofolio/common"
)

type AuthMiddleware struct {
}

func NewAuthMiddleware() *AuthMiddleware {
	return &AuthMiddleware{}
}

func (middleware *AuthMiddleware) GetAuthKey() func(ctx *fiber.Ctx) error {
	return keyauth.New(keyauth.Config{
		KeyLookup: "cookie:access_token",
		Validator: func(ctx *fiber.Ctx, key string) (bool, error) {
			if key == "dslkahjskdhaskudhaisidkjas" { // TODO: move to flag
				return true, nil
			}

			return false, common.NewUnauthorizedError("unauthorized access")
		},
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			// check header if application/json
			accept := ctx.Request().Header.Peek("Accept")
			if string(accept) == "application/json" {
				return common.NewUnauthorizedError("unauthorized access")
			}

			return ctx.Redirect("/admin")
		},
	})
}
